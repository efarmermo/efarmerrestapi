package com.kmware.efarmer.api.client;

import org.apache.http.Header;
import org.apache.http.message.BasicHeader;

/**
 * @author Maxim Maximchuk
 *         date 01-Jul-14.
 */
public enum ContentType {
    JSON, GPX;

    public static final String CONTENT_TYPE = "Content-type";

    public Header getHeader() {
        Header header = null;
        switch (this) {
            case JSON: header = new BasicHeader(CONTENT_TYPE, "application/json"); break;
            case GPX: header = new BasicHeader(CONTENT_TYPE, "application/gpx+xml"); break;
        }
        return header;
    }

}
