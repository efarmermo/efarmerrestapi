package com.kmware.efarmer.api.client;

import com.kmware.efarmer.api.client.http.HttpException;
import com.kmware.efarmer.api.client.http.UnsupportedVersionException;
import com.kmware.efarmer.api.client.oauth.OAuthCredential;
import org.apache.http.HttpResponse;
import org.apache.http.entity.ByteArrayEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Maxim L. Maximchuk
 *         Date: 11/20/13
 */
public class SyncController2Client extends AbstractEFClient {

    private static final String CONTROLLER_NAME = "SyncController2";
    private static final String VERSION = "1.0";

    public SyncController2Client(OAuthCredential credential) {
        super(credential, VERSION);
    }

    /**
     * Generating new URI strings
     *
     * @param count count of URIs string
     * @param type entity type
     * @return json array with URI strings
     */
    public JSONArray getUris(int count, String type) throws IOException, HttpException {
        JSONArray jsonArray;

        Map<String, String> params = new HashMap<String, String>();
        params.put("count", String.valueOf(count));
        params.put("type", type);

        HttpResponse response = executeHttpGet("getURIs", params);
        int statusCode = response.getStatusLine().getStatusCode();
        if (statusCode == 200) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            try {
                String jsonString = reader.readLine();
                jsonArray = new JSONArray(jsonString);
            } catch (JSONException e) {
                e.printStackTrace();
                jsonArray = null;
            } finally {
                reader.close();
            }
        } else if (statusCode == 401) {
            getCredential().refresh();
            jsonArray = getUris(count, type);
        } else {
            throw new HttpException(response);
        }

        return jsonArray;
    }

    /**
     * Defining needed synchronizing operations
     *
     * @param type entity type
     * @param syncDate last sync date
     * @param orgUri organization URI string
     * @return json object with arrays of URI strings for insert, update, delete operations
     */
    public JSONObject synchronize(String type, Date syncDate, String orgUri) throws IOException, HttpException, UnsupportedVersionException {
        JSONObject jsonObject;

        Map<String, String> params = new HashMap<String, String>();
        params.put("type", type);
        params.put("sync_date", String.valueOf(syncDate.getTime()));
        params.put("org_uri", orgUri);

        HttpResponse response = executeHttpGet("synchronize", params);
        int statusCode = response.getStatusLine().getStatusCode();
        if (statusCode == 200) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            try {

                String jsonString = reader.readLine();
                jsonObject = new JSONObject(jsonString);
            } catch (JSONException e) {
                e.printStackTrace();
                jsonObject = null;
            } finally {
                reader.close();
            }
        } else if (statusCode == 401) {
            getCredential().refresh();
            jsonObject = synchronize(type, syncDate, orgUri);
        } else if (statusCode == 406) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String responseString = reader.readLine();
            if (responseString.contains(UnsupportedVersionException.UNSUPPORTED_VERSION_MESSAGE)) {
                throw new UnsupportedVersionException(responseString);
            } else {
                throw new HttpException(response);
            }
        } else {
            throw new HttpException(response);
        }

        return jsonObject;
    }

    /**
     * Getting entity objects
     *
     * @param uriList json array of requesting entities
     * @return json array with entity json objects
     */
    public JSONArray getObjects(JSONArray uriList) throws IOException, HttpException {
        JSONArray jsonArray;

        HttpResponse response = executeHttpPost("getObjects", uriList.toString());
        int statusCode = response.getStatusLine().getStatusCode();
        if (statusCode == 200) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            try {
                String jsonString = reader.readLine();
                jsonArray = new JSONArray(jsonString);
            } catch (JSONException e) {
                e.printStackTrace();
                jsonArray = null;
            } finally {
                reader.close();
            }
        } else if (statusCode == 401) {
            getCredential().refresh();
            jsonArray = getObjects(uriList);
        } else {
            throw new HttpException(response);
        }

        return jsonArray;
    }

    /**
     * Adding entity objects to server
     *
     * @param objectList json array of entity json objects
     * @return response status. "201 Created" if operation successful
     */
    public boolean insert(JSONArray objectList) throws IOException, HttpException {
        HttpResponse response = executeHttpPost("insert", objectList.toString());
        int statusCode = response.getStatusLine().getStatusCode();
        if (statusCode == 201) {
            return true;
        } else if (statusCode == 401) {
            getCredential().refresh();
            return insert(objectList);
        } else {
            throw new HttpException(response);
        }
    }

    /**
     * Updating entity objects on server
     *
     * @param objectList json array of entity json objects
     * @return response status. "202 Accepted" if operation successful
     */
    public boolean update(JSONArray objectList) throws IOException, HttpException {
        HttpResponse response = executeHttpPost("update", objectList.toString());
        int statusCode = response.getStatusLine().getStatusCode();
        if (statusCode == 202) {
            return true;
        } else if (statusCode == 401) {
            getCredential().refresh();
            return update(objectList);
        } else {
            throw new HttpException(response);
        }
    }

    /**
     * Deleting entity objects from server
     *
     * @param uriList json array of URI strings
     * @return response status. "200 Ok" if operation successful
     */
    public boolean delete(JSONArray uriList) throws IOException, HttpException {
        HttpResponse response = executeHttpPost("delete", uriList.toString());
        int statusCode = response.getStatusLine().getStatusCode();
        if (statusCode == 200) {
            return true;
        } else if (statusCode == 401) {
            getCredential().refresh();
            return delete(uriList);
        } else {
            throw new HttpException(response);
        }
    }

    /**
     * Sending gpx data to server
     *
     * @param gpxData gpx xml in bytes
     * @return response status. "202 Accepted" if operation successful
     */
    public boolean sendGpx(byte[] gpxData) throws IOException, HttpException {
        HttpResponse response = executeHttpPost("send_gpx", ContentType.GPX, new ByteArrayEntity(gpxData));
        int statusCode = response.getStatusLine().getStatusCode();
        if (statusCode == 200) {
            return true;
        } else if (statusCode == 401) {
            getCredential().refresh();
            return sendGpx(gpxData);
        } else {
            throw new HttpException(response);
        }
    }

    @Override
    protected String getControllerName() {
        return CONTROLLER_NAME;
    }
}
