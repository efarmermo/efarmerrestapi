package com.kmware.efarmer.api.client;

/**
 * @author Maxim Maximchuk
 *         date 04.12.2014.
 */
public enum AppType {

    EFARMER, EFARMPILOT;

    public String toString() {
        return this.name().toLowerCase();
    }

}
