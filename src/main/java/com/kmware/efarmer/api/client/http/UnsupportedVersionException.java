package com.kmware.efarmer.api.client.http;

/**
 * @author Maxim Maximchuk
 *         date 15.09.2014.
 */
public class UnsupportedVersionException extends Exception {

    public static final String UNSUPPORTED_VERSION_MESSAGE = "Unsupported version: ";
    private String version;

    public UnsupportedVersionException(String responseMessage) {
        super(responseMessage);
        version = responseMessage.replace(responseMessage, UNSUPPORTED_VERSION_MESSAGE);
    }

    public String getVersion() {
        return version;
    }
}
