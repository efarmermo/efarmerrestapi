package com.kmware.efarmer.api.client;

import com.kmware.efarmer.api.client.http.HttpException;
import com.kmware.efarmer.api.client.oauth.OAuthCredential;
import org.apache.http.HttpResponse;
import org.json.JSONObject;

import java.io.IOException;

/**
 * @author Maxim Maximchuk
 *         date 08.10.2014.
 */
public class ShareControllerClient extends AbstractEFClient {

    private static final String CONTROLLER_NAME = "ShareController";

    private static final String RESOURCE_DESCRIPTION = "resource_description";
    private static final String RESOURCE_NAME = "resource _name";
    private static final String URI = "uri";
    private static final String EMAIL = "email";

    public ShareControllerClient(OAuthCredential credential) {
        super(credential);
    }

    public void shareWithFacebook(String resourceUri, String resourceName, String resourceDescription) throws IOException, HttpException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(URI, resourceUri);
        jsonObject.put(RESOURCE_NAME, resourceName);
        jsonObject.put(RESOURCE_DESCRIPTION, resourceDescription);
        checkResponseCode(executeHttpPost("facebook", jsonObject.toString()));
    }

    public void shareWithEmail(String email, String resourceUri, String resourceName, String resourceDescription) throws IOException, HttpException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(EMAIL, email);
        jsonObject.put(URI, resourceUri);
        jsonObject.put(RESOURCE_NAME, resourceName);
        jsonObject.put(RESOURCE_DESCRIPTION, resourceDescription);
        checkResponseCode(executeHttpPost("email", jsonObject.toString()));

    }

    private void checkResponseCode(HttpResponse response) throws HttpException {
        int statusCode = response.getStatusLine().getStatusCode();
        if (statusCode != 200) {
            throw new HttpException(response);
        }
    }

    @Override
    protected String getControllerName() {
        return CONTROLLER_NAME;
    }

}
