package com.kmware.efarmer.api.client;

import com.kmware.efarmer.api.client.http.HttpException;
import com.kmware.efarmer.api.client.oauth.OAuthCredential;
import org.apache.http.HttpResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

/**
 * @author Maxim Maximchuk
 *         date 22.12.14.
 */
public class UserProfileControllerClient extends AbstractEFClient {

    public static final String CONTROLLER_NAME = "user";

    public UserProfileControllerClient(OAuthCredential credential) {
        super(credential);
    }

    public JSONObject getProfile() throws IOException, HttpException {
        JSONObject profile;
        HttpResponse response = executeHttpGet("profile", new HashMap<String, String>());
        int statusCode = response.getStatusLine().getStatusCode();
        if (statusCode == 200) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            try {
                String jsonString = reader.readLine();
                profile = new JSONObject(jsonString);
            } catch (JSONException e) {
                e.printStackTrace();
                profile = null;
            } finally {
                reader.close();
            }
        } else if (statusCode == 401) {
            getCredential().refresh();
            return getProfile();
        } else {
            throw new HttpException(response);
        }
        return profile;
    }

    public boolean setProfile(JSONObject profile) throws IOException, HttpException {
        HttpResponse response = executeHttpPost("profile", profile.toString());
        int statusCode = response.getStatusLine().getStatusCode();
        if (statusCode == 202) {
            return true;
        } else if (statusCode == 401) {
            getCredential().refresh();
            return setProfile(profile);
        } else {
            throw new HttpException(response);
        }
    }

    public JSONArray getFriends() throws IOException, HttpException {
        JSONArray friends;
        HttpResponse response = executeHttpGet("friends", new HashMap<String, String>());
        int statusCode = response.getStatusLine().getStatusCode();
        if (statusCode == 200) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            try {
                String jsonString = reader.readLine();
                friends = new JSONArray(jsonString);
            } catch (JSONException e) {
                e.printStackTrace();
                friends = null;
            } finally {
                reader.close();
            }
        } else if (statusCode == 401) {
            getCredential().refresh();
            return getFriends();
        } else {
            throw new HttpException(response);
        }
        return friends;
    }

    @Override
    protected String getControllerName() {
        return CONTROLLER_NAME;
    }
}
