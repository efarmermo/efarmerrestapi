package com.kmware.efarmer.api.client;

import com.kmware.efarmer.api.client.http.HttpException;
import com.kmware.efarmer.api.client.oauth.OAuthCredential;
import com.maximchuk.rest.client.core.AbstractClient;
import com.maximchuk.rest.client.core.RestApiMethod;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.IOException;
import java.util.Map;

/**
 * @author Maxim L. Maximchuk
 *         Date: 11/21/13
 */
public abstract class AbstractEFClient extends AbstractClient {

    public static final String VERSION_HEADER_NAME = "Version";

    private static final int DEFAULT_TIMEOUT = 30000;

    private OAuthCredential credential;
    private int timeout = DEFAULT_TIMEOUT;
    private String serverUrl;
    private String version;

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    protected AbstractEFClient(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    protected AbstractEFClient(String serverUrl, String version) {
        this(serverUrl);
        this.version = version;
    }

    protected AbstractEFClient(OAuthCredential credential) {
        this(credential.getServerUrl());
        this.credential = credential;
    }

    protected AbstractEFClient(OAuthCredential credential, String version) {
        this(credential.getServerUrl(), version);
        this.credential = credential;
    }

    protected Header createHeader() throws IOException, HttpException {
        if (credential.getRefreshToken() == null) {
            credential.authorize();
        } else if (credential.getAccessToken() == null) {
            credential.refresh();
        }
        return new BasicHeader("Authorization", "Bearer " + credential.getAccessToken());
    }

    @Deprecated
    protected HttpResponse executeHttpGet(String method, Map<String, String> params) throws IOException, HttpException {
        StringBuilder queriedUrlBuilder = new StringBuilder(serverUrl);
        queriedUrlBuilder.append("/RESTService/RESTService/").append(getControllerName()).append("/").append(method);
        if (!params.isEmpty()) {
            queriedUrlBuilder.append("?");
            for (Map.Entry<String, String> param: params.entrySet()) {
                queriedUrlBuilder.append(param.getKey()).append("=").append(param.getValue()).append("&");
            }
            queriedUrlBuilder.deleteCharAt(queriedUrlBuilder.length() - 1);
        }

        HttpGet httpGet = new HttpGet(queriedUrlBuilder.toString());
        if (credential != null) {
            httpGet.setHeader(createHeader());
        }
        return execute(httpGet);
    }

    @Deprecated
    protected HttpResponse executeHttpPost(String method, String jsonBody) throws IOException, HttpException {
        return executeHttpPost(method, ContentType.JSON, new StringEntity(jsonBody, "UTF-8"));
    }

    @Deprecated
    protected HttpResponse executeHttpPost(String method, ContentType contentType, HttpEntity httpEntity) throws IOException, HttpException {
        HttpPost httpPost = new HttpPost(getServiceUrl() + "/" + getControllerName() + "/" + method);

        if (credential != null) {
            httpPost.setHeader(createHeader());
        }
        httpPost.addHeader(contentType.getHeader());
        httpPost.addHeader("charset", "UTF-8");
        httpPost.setEntity(httpEntity);
        return execute(httpPost);
    }

    protected OAuthCredential getCredential() {
        return credential;
    }

    @Deprecated
    private HttpResponse execute(HttpRequestBase httpRequestBase) throws IOException {
        HttpResponse httpResponse = null;

        if (version != null) {
            httpRequestBase.addHeader(new BasicHeader(VERSION_HEADER_NAME, version));
        }

        HttpClient httpClient = new DefaultHttpClient();
        HttpParams params = httpClient.getParams();
        HttpConnectionParams.setConnectionTimeout(params, timeout);
        HttpConnectionParams.setSoTimeout(params, timeout);
        httpResponse = httpClient.execute(httpRequestBase);
        return httpResponse;
    }


    // AbstractClient support

    protected RestApiMethod createApiMethod(String name, RestApiMethod.Type type) throws IOException, HttpException {
        RestApiMethod apiMethod = name != null? new RestApiMethod(name, type): new RestApiMethod(type);
        apiMethod.setTimeout(timeout);
        if (credential != null) {
            apiMethod.addHeader(createHeader());
        }
        return apiMethod;
    }

    protected RestApiMethod createApiMethod(RestApiMethod.Type type) throws IOException, HttpException {
        return createApiMethod(null, type);
    }

    protected boolean processAuthError(com.maximchuk.rest.client.http.HttpException e) throws IOException, HttpException {
        boolean isAuthError = e.getErrorCode() == HttpStatus.SC_UNAUTHORIZED;
        if (isAuthError) {
            getCredential().refresh();
        }
        return isAuthError;
    }

    @Override
    protected String getServiceUrl() {
        return serverUrl + "/RESTService/RESTService";
    }
}
