package com.kmware.efarmer.api.client;

import com.kmware.efarmer.api.client.http.HttpException;
import org.apache.http.HttpResponse;
import org.json.JSONObject;

import java.io.IOException;

/**
 * @author Maxim L. Maximcuhk
 *         Date: 10.04.2014
 */
public class RegistrationServiceClient extends AbstractEFClient {

    private static final String CONTROLLER_NAME = "RegistrationService";
    private static final String TOKEN_KEY = "token";
    private static final String CLIENT_KEY = "client";

    public RegistrationServiceClient(String serverUrl) {
        super(serverUrl);
    }

    public void facebookRegistration(String facebookAccessToken, AppType appType)
            throws IOException, HttpException {
        JSONObject param = new JSONObject();
        param.put(TOKEN_KEY, facebookAccessToken);
        param.put(CLIENT_KEY, appType.toString());
        HttpResponse response = executeHttpPost("facebookRegistration", param.toString());
        int statusCode = response.getStatusLine().getStatusCode();
        if (statusCode != 200) {
            throw new HttpException(response);
        }
    }

    @Override
    protected String getControllerName() {
        return CONTROLLER_NAME;
    }
}
