package com.kmware.efarmer.api.client;

import com.kmware.efarmer.api.client.http.HttpException;
import com.kmware.efarmer.api.client.oauth.OAuthCredential;
import org.apache.http.HttpResponse;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

/**
 * @author Maxim L. Maximchuk
 *         Date: 4/24/14
 */
public class UserControllerClient extends AbstractEFClient {
    private static final String CONTROLLER_NAME = "UserController";
    private static String IMEI_KEY = "IMEI";


    public UserControllerClient(OAuthCredential credential) {
        super(credential);
    }

    public JSONObject getUserData(String imei) throws IOException, HttpException {
        JSONObject jsonObject;

        HashMap<String, String> params = new HashMap<String, String>();
        if (imei != null) {
            params.put(IMEI_KEY, imei);
        }
        HttpResponse response = executeHttpGet("userData", params);
        int statusCode = response.getStatusLine().getStatusCode();
        if (statusCode == 200) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            try {
                String jsonString = reader.readLine();
                jsonObject = new JSONObject(jsonString);
            } catch (JSONException e) {
                e.printStackTrace();
                jsonObject = null;
            } finally {
                reader.close();
            }
        } else {
            throw new HttpException(response);
        }

        return jsonObject;
    }

    public JSONObject getUserData() throws IOException, HttpException {
        return getUserData(null);
    }


    @Override
    protected String getControllerName() {
        return CONTROLLER_NAME;
    }
}
