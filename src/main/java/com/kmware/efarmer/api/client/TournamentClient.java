package com.kmware.efarmer.api.client;

import com.kmware.efarmer.api.client.http.HttpException;
import com.kmware.efarmer.api.client.oauth.OAuthCredential;
import com.maximchuk.rest.client.core.RestApiMethod;
import org.apache.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;

/**
 * @author Maxim Maximchuk
 *         date 20.01.15.
 */
public class TournamentClient extends AbstractEFClient {

    public TournamentClient(OAuthCredential credential) {
        super(credential);
    }

    public JSONArray getTournaments() throws IOException, HttpException {
        try {
            RestApiMethod apiMethod = createApiMethod(RestApiMethod.Type.GET);
            return new JSONArray(executeMethod(apiMethod));
        } catch (com.maximchuk.rest.client.http.HttpException e) {
            if (processAuthError(e)) {
                return getTournaments();
            } else {
                throw new HttpException(e);
            }
        }
    }

    @Deprecated
    public JSONArray getAvailableTournaments() throws IOException, HttpException {
        return getTournaments();
    }

    public JSONArray getAcceptedTournaments() throws IOException, HttpException {
        try {
            RestApiMethod apiMethod = createApiMethod("accepted", RestApiMethod.Type.GET);
            return new JSONArray(executeMethod(apiMethod));
        } catch (com.maximchuk.rest.client.http.HttpException e) {
            if (processAuthError(e)) {
                return getAcceptedTournaments();
            } else {
                throw new HttpException(e);
            }
        }
    }

    public JSONObject getCurrentTournament() throws IOException, HttpException {
        try {
            RestApiMethod apiMethod = createApiMethod("current", RestApiMethod.Type.GET);
            String res = executeMethod(apiMethod);
            return apiMethod.getStatusLine().getStatusCode() == HttpStatus.SC_OK ?
                    new JSONObject(res) : null;
        } catch (com.maximchuk.rest.client.http.HttpException e) {
            if (processAuthError(e)) {
                return getCurrentTournament();
            } else {
                throw new HttpException(e);
            }
        }
    }

    public JSONObject checkTournamentEnd(String tournamentUri) throws IOException, HttpException {
        try {
            RestApiMethod apiMethod = createApiMethod("endcheck", RestApiMethod.Type.POST);
            apiMethod.setStringData(tournamentUri);
            String res = executeMethod(apiMethod);
            return apiMethod.getStatusLine().getStatusCode() == HttpStatus.SC_OK ?
                    new JSONObject(res) : null;
        } catch (com.maximchuk.rest.client.http.HttpException e) {
            if (processAuthError(e)) {
                return checkTournamentEnd(tournamentUri);
            } else {
                throw new HttpException(e);
            }
        }
    }

    public boolean acceptTournament(String tournamentUri) throws IOException, HttpException {
        try {
            RestApiMethod apiMethod = createApiMethod("accept", RestApiMethod.Type.POST);
            apiMethod.setStringData(tournamentUri);
            executeMethod(apiMethod);
            return apiMethod.getStatusLine().getStatusCode() == HttpStatus.SC_OK;
        } catch (com.maximchuk.rest.client.http.HttpException e) {
            if (processAuthError(e)) {
                return acceptTournament(tournamentUri);
            } else {
                throw new HttpException(e);
            }
        }
    }

    public boolean rejectTournament(String tournamentUri) throws IOException, HttpException {
        try {
            RestApiMethod apiMethod = createApiMethod("reject", RestApiMethod.Type.POST);
            apiMethod.setStringData(tournamentUri);
            executeMethod(apiMethod);
            return apiMethod.getStatusLine().getStatusCode() == HttpStatus.SC_OK;
        } catch (com.maximchuk.rest.client.http.HttpException e) {
            if (processAuthError(e)) {
                return rejectTournament(tournamentUri);
            } else {
                throw new HttpException(e);
            }
        }
    }

    @Override
    protected String getControllerName() {
        return "tournament";
    }

}
