package com.kmware.efarmer.api.client;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;

import static org.junit.Assert.*;

public class UserProfileControllerClientTest extends AbstractTestWithCredential {

    public static final String LASTNAME_KEY = "lastName";

    @Test
    public void testGetProfile() throws Exception {
        try {
            UserProfileControllerClient client = new UserProfileControllerClient(getCredential());
            JSONObject profile = client.getProfile();
            assertNotNull(profile);
            System.out.println(profile.toString());
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testSetProfile() throws Exception {
        try {
            UserProfileControllerClient client = new UserProfileControllerClient(getCredential());
            JSONObject profile = client.getProfile();

            String testLastname = "testName";
            String lastname = profile.getString(LASTNAME_KEY);

            profile.put(LASTNAME_KEY, testLastname);
            client.setProfile(profile);
            profile = client.getProfile();
            assertEquals(testLastname, profile.getString(LASTNAME_KEY));
            profile.put(LASTNAME_KEY, lastname);
            client.setProfile(profile);
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testGetFriends() throws Exception {
        try {
            UserProfileControllerClient client = new UserProfileControllerClient(getCredential());
            JSONArray friends = client.getFriends();
            assertNotNull(friends);
            System.out.println(friends.toString());
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }
}