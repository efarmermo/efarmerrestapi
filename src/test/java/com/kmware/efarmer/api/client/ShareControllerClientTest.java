package com.kmware.efarmer.api.client;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ShareControllerClientTest extends AbstractTestWithCredential {

    private static final String URI = "content://DAYBOOK_OPERATION/e00ee630-bc93-42aa-902c-343b154bdadb";
    private static final String RESOURCE_NAME = "Посев";
    private static final String RESOURCE_DESCRIPTION = "Посев озимой";
    private static final String EMAIL = "test_mail@gmail.com";

    private ShareControllerClient apiClient;

    @Before
    public void setUp() {
        apiClient = new ShareControllerClient(getCredential());
    }

    @Test
    public void testShareWithFacebook() {
        try {
            apiClient.shareWithFacebook(URI, RESOURCE_NAME, RESOURCE_DESCRIPTION);
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void testShareWithEmail() {
        try {
            apiClient.shareWithEmail(EMAIL, URI, RESOURCE_NAME, RESOURCE_DESCRIPTION);
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }
}