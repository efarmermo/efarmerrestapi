package com.kmware.efarmer.api.client;

import com.kmware.efarmer.api.client.oauth.OAuthCredential;
import com.kmware.efarmer.api.client.oauth.OAuthTokenEvent;
import com.kmware.efarmer.api.client.oauth.OAuthTokenListener;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Maxim L. Maximchuk
 *         Date: 11/20/13
 */
public class OAuthCredentialTest {

    private OAuthCredential credential;

    @Before
    public void setUp() {
        credential = OAuthCredential.builder()
                .clientId(Constants.CLIENT_ID)
                .clientSecret(Constants.CLIENT_SECRET)
                .redirectUri(Constants.REDIRECT_URI)
                .serverUrl(Constants.SERVER_URL)
                .username(Constants.USERNAME)
                .password(Constants.PASSWORD)
                .build();
    }

    @Test
    public void testAuthorize() {
        try {
            credential.authorize();
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testAuthorizeSocial() {
        try {
            credential = OAuthCredential.builder()
                    .clientId(Constants.CLIENT_ID)
                    .clientSecret(Constants.CLIENT_SECRET)
                    .redirectUri(Constants.REDIRECT_URI)
                    .serverUrl(Constants.SERVER_URL)
                    .facebookToken(Constants.FACEBOOK_TOKEN)
                    .build();
            List<OAuthCredential.OAuthUser> users = credential.authorizeSocial();
            assertFalse(users.isEmpty());
            credential.authorize(users.get(0));
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testRefresh() {
        try {
            credential.authorize();
            credential.refresh();
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testListener() {
        try {
            credential.addListener(tokenListener);

            credential.authorize();
            assertTrue(isAuthListenerOk);

            credential.refresh();
            assertTrue(isRefreshListenerOk);
        } catch (Exception e) {
           fail(e.getMessage());
        }
    }

    private boolean isAuthListenerOk = false;
    private boolean isRefreshListenerOk = false;

    private OAuthTokenListener tokenListener = new OAuthTokenListener() {

        @Override
        public void authorize(OAuthTokenEvent event) {
            if (event.getRefreshToken() != null
                    && event.getAccessToken() != null
                    && event.getExpires() != null) {
                isAuthListenerOk = true;
            }
        }

        @Override
        public void refreshToken(OAuthTokenEvent event) {
            if (event.getRefreshToken() != null
                    && event.getAccessToken() != null
                    && event.getExpires() != null) {
                isRefreshListenerOk = true;
            }
        }
    };

}
