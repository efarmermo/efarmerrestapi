package com.kmware.efarmer.api.client;

import mobi.efarmer.gpx.Gpx;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import static org.junit.Assert.*;

/**
 * @author Maxim L. Maximcuhk
 *         Date: 20.11.13
 */
public class SyncController2ClientTest extends AbstractTestWithCredential {

    private SyncController2Client apiClient;

    @Before
    public void setUp() {
        apiClient = new SyncController2Client(getCredential());
    }
    
    @Test
    public void testGetUris() {
        try {
            JSONArray res = apiClient.getUris(15, "ORGANIZATION");
            System.out.println(res.toString());
            assertEquals(res.length(), 15);
        } catch (Exception e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
    }

    @Test
    public void testSynchronize() {
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            JSONObject res = apiClient.synchronize("ORGANIZATION_FIELD", dateFormat.parse("2010-01-01"), "content://ORGANIZATION/15");
            assertNotNull(res);
            System.out.println(res.toString());
        } catch (Exception e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
    }

    @Test
    public void testGetObjects() {
        try {
            JSONArray uriList = new JSONArray();
            uriList.put("content://ORGANIZATION_FIELD/2");
            uriList.put("content://ORGANIZATION_FIELD/3");
            JSONArray res = apiClient.getObjects(uriList);
            System.out.println(res.toString());
            assertTrue(res.length() > 0);
        } catch (Exception e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
    }

    @Test
    public void testInsert() {
        try {
            JSONObject orgJsonObject = new JSONObject("{\"currentArea\":5555,\"startDate\":\"2010-11-29\",\"contour\":false,\"organization\":\"content://ORGANIZATION/15\",\"isHandMeasured\":false,\"description\":\"\",\"name\":\"test22\",\"endDate\":\"3554-06-09\",\"cadastral\":0,\"orgRootId\":50,\"uri\":\"content://ORGANIZATION_FIELD/2221\"}");
            JSONArray objectList = new JSONArray();
            objectList.put(orgJsonObject);
            assertTrue(apiClient.insert(objectList));
        } catch (Exception e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
    }

    @Test
    public void testUpdate() {
        try {
            JSONObject orgJsonObject = new JSONObject("{\"currentArea\":2234,\"uri\":\"content://ORGANIZATION_FIELD/2221\"}");
            JSONArray objectList = new JSONArray();
            objectList.put(orgJsonObject);
            assertTrue(apiClient.update(objectList));
        } catch (Exception e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
    }

    @Test
    public void testDelete() {
        try {
            JSONArray uriList = new JSONArray();
            uriList.put("content://ORGANIZATION_FIELD/2221");
            assertTrue(apiClient.delete(uriList));
        } catch (Exception e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
    }

    @Test
    public void testSendGpx() {
        try {
            Gpx gpx = Gpx.read(getClass().getClassLoader().getResourceAsStream("track.gpx"));
            assertTrue(apiClient.sendGpx(gpx.serialize()));
        } catch (Exception e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
    }
    
}
