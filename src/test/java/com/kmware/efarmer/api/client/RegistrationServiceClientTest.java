package com.kmware.efarmer.api.client;

import com.kmware.efarmer.api.client.http.HttpException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

/**
 * @author Maxim L. Maximchuk
 *         Date: 4/24/14
 */
public class RegistrationServiceClientTest {

    private RegistrationServiceClient apiClient;

    @Before
    public void setUp() {
        apiClient = new RegistrationServiceClient(Constants.SERVER_URL);
    }

    @Test
    public void testFacebookRegistration() {
        try {
            apiClient.facebookRegistration(Constants.FACEBOOK_TOKEN, AppType.EFARMPILOT);
        } catch (IOException e) {
            Assert.fail(e.getMessage());
        } catch (HttpException e) {
            Assert.fail(e.getMessage());
        }
    }

}
