package com.kmware.efarmer.api.client;

import com.kmware.efarmer.api.client.http.HttpException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

public class TournamentClientTest extends AbstractTestWithCredential {

    private TournamentClient client;

    @Before
    public void setUp() throws Exception {
        client = new TournamentClient(getCredential());
    }

    @Test
    public void testGetAvailableTournaments() throws Exception {
        try {
            JSONArray res = client.getTournaments();
            System.out.println(res.toString());
            Assert.assertTrue(res.length() > 0);
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void testAcceptTournament() throws Exception {
        try {
            String uri = getAvailableTournamentUri();
            boolean res = client.acceptTournament(uri);
            Assert.assertTrue(res);
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        } finally {
            client.rejectTournament(getAcceptedTournamentUri());
        }
    }

    @Test
    public void testGetAcceptedTournaments() throws Exception {
        try {
            client.acceptTournament(getAvailableTournamentUri());
            JSONArray res = client.getAcceptedTournaments();
            System.out.println(res.toString());
            Assert.assertTrue(res.length() > 0);
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        } finally {
            client.rejectTournament(getAcceptedTournamentUri());
        }
    }

    @Test
    public void testGetCurrentTournament() throws Exception {
        try {
            client.acceptTournament(getAvailableTournamentUri());
            JSONObject res = client.getCurrentTournament();
            Assert.assertNotNull(res);
            System.out.println(res.toString());
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        } finally {
            client.rejectTournament(getAcceptedTournamentUri());
        }
    }

    @Test
    public void testCheckTournamentEnd() throws Exception {
        try {
            JSONObject res = client.checkTournamentEnd("content://TOURNAMENT/2d93e29f-ebf1-4dc3-8e8a-7a351e686099");
            Assert.assertNotNull(res);
            System.out.println(res.toString());
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void testRejectTournament() throws Exception {
        try {
            client.acceptTournament(getAvailableTournamentUri());
            boolean res = client.rejectTournament(getAcceptedTournamentUri());
            Assert.assertTrue(res);
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    private String getAvailableTournamentUri() throws IOException, HttpException {
        JSONArray tournaments = client.getTournaments();
        Assert.assertFalse("Available tournaments is not found", tournaments.length() == 0);
        return tournaments.getJSONObject(0).getString("uri");
    }

    private String getAcceptedTournamentUri() throws IOException, HttpException {
        JSONArray tournaments = client.getAcceptedTournaments();
        Assert.assertFalse("Accepted tournaments is not found", tournaments.length() == 0);
        return tournaments.getJSONObject(0).getString("uri");
    }
}