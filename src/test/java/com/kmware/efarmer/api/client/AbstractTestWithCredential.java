package com.kmware.efarmer.api.client;

import com.kmware.efarmer.api.client.oauth.OAuthCredential;

/**
 * @author Maxim Maximchuk
 *         date 08.10.2014.
 */
public class AbstractTestWithCredential {

    public OAuthCredential getCredential() {
        return OAuthCredential.builder()
                .clientId(Constants.CLIENT_ID)
                .clientSecret(Constants.CLIENT_SECRET)
                .redirectUri(Constants.REDIRECT_URI)
                .serverUrl(Constants.SERVER_URL)
                .username(Constants.USERNAME)
                .password(Constants.PASSWORD)
                .build();
    }
}
